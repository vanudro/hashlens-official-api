package nl.hashlens.api.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class UserPasswordOldBody extends UserPasswordBody {

    private String old_password;

    public void setOldPassword(String old_password) {
        this.old_password = old_password;
    }

    public String getOldPassword() {
        return this.old_password;
    }
}
