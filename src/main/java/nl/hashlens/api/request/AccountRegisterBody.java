package nl.hashlens.api.request;

public class AccountRegisterBody extends UserFullBody {

    private String link;

    public String getLink() {
        return this.link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
