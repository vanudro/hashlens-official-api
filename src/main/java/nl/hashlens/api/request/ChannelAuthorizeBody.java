package nl.hashlens.api.request;

public class ChannelAuthorizeBody {

    private String link;

    public String getLink() {
        return this.link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
