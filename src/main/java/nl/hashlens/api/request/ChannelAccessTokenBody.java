package nl.hashlens.api.request;

public class ChannelAccessTokenBody {

    private String token;

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
