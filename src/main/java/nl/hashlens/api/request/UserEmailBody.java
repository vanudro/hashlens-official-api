package nl.hashlens.api.request;

public class UserEmailBody {

    private String email;

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
