package nl.hashlens.api.request;

public class UserFullBody extends UserNamesBody {

    private String password;

    public String getPassword() {
        return this.password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
