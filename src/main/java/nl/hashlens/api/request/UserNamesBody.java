package nl.hashlens.api.request;

public class UserNamesBody extends UserEmailBody {

    private String first_name, last_name;

    public String getFirstName() {
        return this.first_name;
    }
    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }
    public String getLastName() {
        return this.last_name;
    }
    public void setLastName(String last_name) {
        this.last_name = last_name;
    }
}
