package nl.hashlens.api.components;

import nl.hashlens.api.asset.Console;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Map;

@Component
public class Mail {

    private static JavaMailSender sender;
    private static TemplateEngine engine;

    // TODO: Email on acid

    @Autowired
    public Mail(JavaMailSender sender, TemplateEngine engine) {
        Mail.sender = sender;
        Mail.engine = engine;
        Console.info("init mail service");
    }

    public static String getTemplate(String template, Map<String, String> variables) {
        Context context = new Context();
        if (variables.size() > 0) {
            for (Map.Entry<String, String> entry : variables.entrySet()) {
                context.setVariable(entry.getKey(), entry.getValue());
            }
        }
        return engine.process(template, context);
    }

    public static boolean send(String to_address, String to_name, String subject, String content, boolean is_html) {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        String from_address = Properties.get(Properties.APP_MAIL);
        String from_name = Properties.get(Properties.APP_NAME);

        if (from_address == null) {
            Console.info("Couldn't find address");
            return false;
        }
        if (from_name == null) {
            Console.info("Couldn't find name");
            return false;
        }

        try {
            helper.setFrom(new InternetAddress(from_address, from_name));
            helper.setTo(new InternetAddress(to_address, to_name));
            helper.setText(content, is_html);
            helper.setSubject(subject);

            Console.info(String.format("Sending mail to <%s>%s from <%s>%s with subject \"%s\"", to_name, to_address, from_name, from_address, subject));
            sender.send(message);
            return true;
        } catch (MessagingException | UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void sendEmail() throws Exception {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setFrom(new InternetAddress("vanudro@cronos.be", "Hashlens"));
        helper.setTo("robinvanuden@gmail.com");
        helper.setText("How are you?");
        helper.setSubject("Hi");

        message.setText("Hello there");

        sender.send(message);
    }
}
