package nl.hashlens.api.components;

import nl.hashlens.api.asset.Console;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
public class Properties {

    public static final String APP_NAME = "spring.application.name";
    public static final String APP_MAIL = "spring.mail.username";

    public static final String FACEBOOK_AUTH_URL = "hashlens.channels.facebook.auth_url";
    public static final String INSTAGRAM_AUTH_URL = "hashlens.channels.instagram.auth_url";
    public static final String LINKEDIN_AUTH_URL = "hashlens.channels.linkedin.auth_url";

    private static Environment environment = null;

    @Autowired
    public Properties(Environment environment) {
        Properties.environment = environment;
        Console.info("init application properties");
    }

    public static String get(String key) {
        return get(key, null);
    }

    public static String get(String key, String default_value) {
        return environment.getProperty(key, default_value);
    }
}