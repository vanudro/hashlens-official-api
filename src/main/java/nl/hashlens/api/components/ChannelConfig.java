package nl.hashlens.api.components;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:channels.properties")
public class ChannelConfig {}