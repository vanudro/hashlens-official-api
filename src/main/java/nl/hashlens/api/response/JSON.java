package nl.hashlens.api.response;

import nl.hashlens.api.asset.Console;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JSON {

    private JSONObject object = null;

    public JSON(String raw) {
        try {
            this.object = new JSONObject(raw);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private JSON(JSONObject raw) {
        this.object = raw;
    }

    public boolean isEmpty() {
        return (this.object == null);
    }

    public int size() {
        return this.object.length();
    }

    public String getString(String key, Object return_value) {
        try {
            return this.object.getString(key);
        } catch (JSONException e) {
            Console.warning(String.format("Key \"%s\" was not found in: {%s}", key, this.object.toString()));
            return return_value.toString();
        }
    }

    public String getString(String key) {
        return this.getString(key, null);
    }

    public List<JSON> getArray(String key) {
        List<JSON> list = new ArrayList<>();
        try {
            JSONArray array = this.object.getJSONArray(key);
            for (int i = 0; i < array.length(); i++) {
                JSON json = JSON.decode(array.getJSONObject(i).toString(4));
                list.add(json);
            }
        } catch (JSONException e) {
            Console.warning(String.format("Array \"%s\" was not found in: {%s}", key, this.object.toString()));
        } catch (Exception e) {
            Console.warning(String.format("Something went wrong while converting array: \"%s\"", key));
        }
        return list;
    }

    public JSON getObject(String key) {
        JSON json = null;
        try {
            json = JSON.decode(this.getString(key));
        } catch (Exception e){
            Console.warning(String.format("Object \"%s\" was not found in: {%s}", key, this.object.toString()));
        }
        return json;
    }

    public static JSON decode(String raw_body) {
        return new JSON(raw_body);
    }

    public static JSON decode(JSONObject raw_body) {
        return new JSON(raw_body);
    }

    public static String encode(Map data) {
        return new JSONObject(data).toString();
    }

    public static String encode(String string) {
        try {
            return new JSONObject(string).toString(4);
        } catch (Exception e) {
            System.out.println("JSON.encode ERROR");
            System.out.println(e.getMessage());
            return null;
        }
    }

    @Override
    public String toString() {
        return "JSON{object=" + this.object + '}';
    }
}