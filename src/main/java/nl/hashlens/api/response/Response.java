package nl.hashlens.api.response;

import nl.hashlens.api.asset.Console;
import nl.hashlens.api.asset.Date;

import java.util.*;

public class Response {


    public enum MessageType {Error, Warning, Info, Success}

    private String message;
    private List<Map<String, String>> messages;
    private int data_count = 0;
    private Map<String, Object> data;
    private int errors, warnings;

    public Response() {
        this.messages = new ArrayList<>();
        this.data = new HashMap<>();
    }

    public boolean hasErrors() {
        return (this.errors > 0 || this.warnings > 0);
    }

    // Add message
    // ------------------------------------------
    public void addMessage(MessageType type, String message, String field) {
        String log = (field != null && !Objects.equals(field, "")) ? String.format("%s \"%s\"", message, field) : message;
        switch (type) {
            case Error:
                Console.error(log);
                this.errors++;
                break;
            case Warning:
                Console.warning(log);
                this.warnings++;
                break;
            case Success:
                Console.success(log);
                break;
            case Info:
                Console.info(log);
                break;

        }
        this.messages.add(new ResponseMessage(type.toString().toLowerCase(), message, field).getMessage());
    }
    public void addMessage(MessageType type, String message) {
        this.addMessage(type, message, "");
    }

    // Add ERROR
    // ------------------------------------------

    public void addError(String message, String field) {
        this.addMessage(MessageType.Error, message, field);
    }

    public void addError(String message) {
        this.addMessage(MessageType.Error, message);
    }

    // Add success
    // ------------------------------------------
    public void addSuccess(String message, String field) {
        this.addMessage(MessageType.Success, message, field);
    }

    public void addSuccess(String message) {
        this.addMessage(MessageType.Success, message);
    }

    // Add Warning
    // ------------------------------------------
    public void addWarning(String message, String field) {
        this.addMessage(MessageType.Warning, message, field);
    }

    public void addWarning(String message) {
        this.addMessage(MessageType.Warning, message);
    }

    // Add Info
    // ------------------------------------------
    public void addInfo(String message, String field) {
        this.addMessage(MessageType.Info, message, field);
    }

    public void addInfo(String message) {
        this.addMessage(MessageType.Info, message);
    }

    public void addData(Object data) {
        this.addData(String.valueOf(this.data_count), data);
    }

    public void addData(String key, Object data) {
        this.data.put(key, data);
        this.data_count++;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public String get() {
        Map<String, Object> map = new HashMap<>();
        map.put("status", (this.hasErrors()) ? "ERROR" : "OK");
        map.put("timestamp", new Date().getOutput());
        map.put("message", this.message);
        map.put("messages", this.messages);
        map.put("data", this.data);

        return JSON.encode(map);
    }


    public void nonJsonError() {
        this.addError("Unknown call. Please read the documentation for proper use.");
    }

    public void unknownPost() {
        this.addError("Unknown call. Please read the documentation for proper use.");
    }

    public void exceptionError() {
        this.addError("Something went wrong... Please contact the administrators!");
    }

    public void changesSaved() {
        this.addSuccess("All changes saved!");
    }

    public void notEmail(String field) {
        this.addMessage(MessageType.Error, String.format("\"%s\" does not appear to be a valid email address...", field), field);
    }

    public void notFound(String name_of_object) {
        this.addMessage(MessageType.Warning, String.format("\"%s\" not found...", name_of_object));
    }

    public void errorWhileSaving() {
        this.addError("Error while saving...");
    }

    public void missingParameter(String param) {
        this.missingParameter(param, false);
    }

    public void missingParameter(String param, boolean in_url) {
        this.addMessage(
                MessageType.Error,
                String.format("Missing parameters! Please make sure you add '%s' in your %s.", (in_url) ? param + "={value}" : param, (in_url) ? "url" : "call"),
                param
        );
    }

    public void emptyField(String param) {
        this.addMessage(
                MessageType.Error,
                "This field can't be empty",
                param
        );
    }
}
