package nl.hashlens.api.response;

import java.util.HashMap;
import java.util.Map;

class ResponseMessage {

    private String type, message, field;

    ResponseMessage(String type, String message, String field) {
        this.type = type;
        this.message = message;
        this.field = field;
    }

    Map<String, String> getMessage() {
        Map<String, String> map = new HashMap<>();
        map.put("type", this.type);
        map.put("message", this.message);
        map.put("field", this.field);
        return map;
    }
}
