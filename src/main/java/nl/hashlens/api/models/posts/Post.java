package nl.hashlens.api.models.posts;

import nl.hashlens.api.response.JSON;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Post {

    private String ID, platform, description, date, url;
    private int likes, comments;
    private List<Object> media;
    private PostUser user;

    public Post(String ID, String platform, String description, String date, String url, int likes, int comments) {
        this.media = new ArrayList<>();
        this.ID = ID;
        this.platform = platform;
        this.description = description;
        this.date = date;
        this.url = url;
        this.likes = likes;
        this.comments = comments;
    }

    public String getID() {
        return this.ID;
    }
    public void setID(String ID) {
        this.ID = ID;
    }
    public String getPlatform() {
        return this.platform;
    }
    public void setPlatform(String platform) {
        this.platform = platform;
    }
    public String getDescription() {
        return this.description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDate() {
        return this.date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getUrl() {
        return this.url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public int getLikes() {
        return this.likes;
    }
    public void setLikes(int likes) {
        this.likes = likes;
    }
    public int getComments() {
        return this.comments;
    }
    public void setComments(int comments) {
        this.comments = comments;
    }
    public List<Object> getMedia() {
        return this.media;
    }
    public void addMedia(Object media) {
        this.media.add(media);
    }
    public PostUser getUser() {
        return this.user;
    }
    public void setUser(PostUser user) {
        this.user = user;
    }

    public Map<String, Object> getMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", this.getID());
        map.put("platform", this.getPlatform());
        map.put("description", this.getDescription());
        map.put("date", this.getDate());
        map.put("url", this.getUrl());
        map.put("likes", this.getLikes());
        map.put("comments", this.getComments());
        map.put("media", this.getMedia());
        map.put("user", this.getUser().getMap());
        return map;
    }
}