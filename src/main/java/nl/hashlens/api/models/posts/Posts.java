package nl.hashlens.api.models.posts;

import nl.hashlens.api.asset.Console;
import nl.hashlens.api.asset.Date;
import nl.hashlens.api.models.channels.Channel;
import nl.hashlens.api.response.JSON;
import org.apache.commons.io.IOUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class Posts {

    public static Map<String, Post> findByChannel(Channel channel) {
        String url;
        switch (channel.getType()) {
            case INSTAGRAM:
                url = String.format("https://api.instagram.com/v1/users/self/media/recent?access_token=%s", channel.getAccessToken());
                break;
            case FACEBOOK:
                String raw = "https://graph.facebook.com/me?fields=id,name,posts%7Blink,width,height,type,description,source,full_picture,created_time,likes.limit%281%29,comments.limit%281%29%7D";
                url = String.format("%s&access_token=%s", raw, channel.getAccessToken());
                break;
            default:
                url = null;
                break;
        }
        return getPosts(channel, url);
    }

    public static JSON getFromChannel(String url) {
        JSON json = JSON.decode("{}");

        if (url == null) return json;

        Console.info(String.format("Connecting to %s", url));
        String body = null;
        try {
            RestTemplate restTemplate = new RestTemplate();

            ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                body = responseEntity.getBody();
            } else {
                Console.error(String.format("Got an error response from '%s'...", url));
                Console.error(responseEntity.getStatusCode().toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Console.error(String.format("Something went wrong while connecting to '%s'...", url));
        }

        json = JSON.decode(body);
        if (!json.isEmpty()) {
            Console.print(body);
        }
        return json;
    }

    public static Map<String, Post> getPosts(Channel channel, String url) {
        Map<String, Post> posts = new HashMap<>();
        JSON json = getFromChannel(url);
        switch (channel.getType()) {
            case INSTAGRAM:
                posts = getPostsFromIG(json);
                break;
            case FACEBOOK:
                posts = getPostsFromFacebook(json);
                break;
        }

        return posts;
    }

    public static Map<String, Post> getPostsFromFacebook(JSON raw_json) {
        Map<String, Post> posts = new HashMap<>();
        Console.print(raw_json.toString());
        return posts;
    }

    public static Map<String, Post> getPostsFromIG(JSON raw_json) {
        Console.print(raw_json.toString());
        List<JSON> json_array = raw_json.getArray("data");
        Map<String, Post> posts = new HashMap<>();

        PostUser user = null;

        if (json_array != null) {
            for (JSON json : json_array) {

                String id = json.getString("id");
                String created_time = json.getString("created_time");
                String link = json.getString("link");

                long timestamp = Long.parseLong(created_time) * 1000;
                Date date = new Date("dd MMM yyyy", timestamp);
                Date id2 = new Date("yyyyMMddHHmmss", timestamp);
                String description = json.getObject("caption").getString("text");
                String likes = json.getObject("likes").getString("count");
                String comments = json.getObject("comments").getString("count");

                Post post = new Post(
                        id,
                        "instagram",
                        description,
                        date.getOutput(),
                        link,
                        Integer.parseInt(likes),
                        Integer.parseInt(comments)
                );
                Console.print("POST: " + id);
                Console.line();

                JSON user_object = json.getObject("user");

                if (user == null && user_object != null) {
                    String full_name = user_object.getString("full_name");
                    String user_name = user_object.getString("username");
                    String profile_picture = user_object.getString("profile_picture");
                    String url = String.format("https://www.instagram.com/%s/", user_name);

                    String image_data = profile_picture;
                    try {
                        byte[] imageContent = IOUtils.toByteArray(new URL(profile_picture));
//                        image_data = Base64.encodeBase64String(imageContent);
//                        image_data = String.format("data:image/png;base64,%s", image_data);
                    } catch (Exception e) {

                    }

                    user = new PostUser(user_name, full_name, image_data, url);
                }
                post.setUser(user);

                List<JSON> media_array = json.getArray("carousel_media");
                if (!media_array.isEmpty()) {
                    Console.print("CAROUSEL MEDIA");
                    Console.print(media_array.toString());
//                    Console.print(media_array.toString());
                    int count = 0;
                    for (JSON media_json : media_array) {

                        count++;
                        Console.print("ITEM: " + count);
                        Console.print("");
                        Console.print(media_array.toString());
                        String type = "image";

                        JSON media = media_json.getObject("images");
                        if (media == null || media.isEmpty()) {
                            media = media_json.getObject("videos");
                            type = "video";
                        }
                        PostMedia post_media = createPostMedia(media, type);
                        post.addMedia(post_media.getMap());


                    }
                    Console.print("MEDIA COUNT: " + count);
                } else {
                    String type = "video";
                    JSON media = json.getObject("videos");
                    if (media == null || media.isEmpty()) {
                        media = json.getObject("images");
                        type = "image";
                    }
                    Console.print("MEDIA " + type.toUpperCase());
                    Console.print(media.toString());
                    PostMedia post_media = createPostMedia(media, type);
                    post.addMedia(post_media.getMap());
                }
                posts.put(id2.getOutput(), post);
            }
        }

        return posts;
    }

    private static PostMedia createPostMedia(JSON media, String type) {
        PostMedia post_media = null;
        if (media != null && !media.isEmpty()) {
            media = media.getObject("standard_resolution");
            String url_string = media.getString("url", "none");
            String width = media.getString("width");
            String height = media.getString("height");

            String image_data = url_string;
            try {
                URL url = new URL(url_string);
                Console.print("Retrieve image from:");
                Console.print(url_string);

                // Convert image to Base64 encode for offline images
//                byte[] imageContent = IOUtils.toByteArray(new URL(url_string));
//                image_data = Base64.encodeBase64String(imageContent);
//
//                String ext = "png";
//                if (Objects.equals(type, "video")) {
//                    ext = "mp4";
//                }
//                image_data = String.format("data:%s/%s;base64,%s", type, ext, image_data);

//                BufferedInputStream bis = new BufferedInputStream(url.openConnection().getInputStream());
//                byte[] imageData = new byte[2048];
//                bis.read(imageData);
//
//                // Converting Image byte array into Base64 String
//                image_data = encodeImage(imageData);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            post_media = new PostMedia(
                    image_data,
                    type,
                    width,
                    height,
                    url_string
            );
        }
        return post_media;
    }


    public static String encodeImage(byte[] imageByteArray) {
        return Base64.encodeBase64URLSafeString(imageByteArray);
    }

}
