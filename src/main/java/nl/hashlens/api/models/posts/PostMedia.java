package nl.hashlens.api.models.posts;

import java.util.HashMap;
import java.util.Map;

public class PostMedia {

    public String content, type, width, height, url;

    PostMedia(String content, String type, String width, String height, String url) {
        this.content = content;
        this.type = type;
        this.width = width;
        this.height = height;
        this.url = url;
    }

    public String getContent() {
        return this.content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getWidth() {
        return this.width;
    }
    public void setWidth(String width) {
        this.width = width;
    }
    public String getHeight() {
        return this.height;
    }
    public void setHeight(String height) {
        this.height = height;
    }
    public String getUrl() {
        return this.url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, Object> getMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("content", this.getContent());
        map.put("type", this.getType());
        map.put("width", this.getWidth());
        map.put("height", this.getHeight());
        map.put("url", this.getUrl());
        return map;
    }

}
