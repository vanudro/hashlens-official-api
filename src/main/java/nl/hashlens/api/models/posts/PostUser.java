package nl.hashlens.api.models.posts;

import java.util.HashMap;
import java.util.Map;

public class PostUser {

    private String username, name, image, url;

    PostUser(String username, String name, String image, String url) {
        this.username = username;
        this.name = name;
        this.image = image;
        this.url = url;
    }

    public String getUsername() {
        return this.username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getImage() {
        return this.image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public String getUrl() {
        return this.url;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public Map<String, Object> getMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("username", this.getUsername());
        map.put("full_name", this.getName());
        map.put("image", this.getImage());
        map.put("url", this.getUrl());
        return map;
    }
}
