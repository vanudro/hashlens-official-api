package nl.hashlens.api.models;

import java.util.Objects;
import java.util.UUID;

public class DataModel {

    private String id;
    private String ID;

    protected DataModel() {

    }

    public DataModel(String ID) {
        this.ID = ID;
    }

    public String getId() {
        return this.id;
    }

    public boolean isValid() {
        return (this.ID != null);
    }

    public String getID() {
        return this.ID;
    }

    protected void setNewID() {
        this.setID(this.newID());
    }

    private void setID(String ID) {
        this.ID = ID;
    }
    public boolean matchesID(String ID) {
        return (Objects.equals(this.getID(), ID));
    }

    private String newID() {
        return UUID.randomUUID().toString();
    }
}
