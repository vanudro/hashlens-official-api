package nl.hashlens.api.models.users;

import nl.hashlens.api.asset.Console;
import nl.hashlens.api.asset.Date;
import nl.hashlens.api.asset.Hash;
import nl.hashlens.api.models.DataModel;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Objects;

public class User extends DataModel {

    private String first_name, last_name, email, password, password_reset;
    private boolean active = false;

    private User() {}

    /**
     * Constructor used to make an object of a user from raw data
     * @param first_name First name of a user
     * @param last_name  Last name of a user
     * @param email      the user's email address
     * @param password   the user's encrypted password
     */

    User(String first_name, String last_name, String email, String password) {
        this.setNewID();
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.setPassword(password);
    }

    /**
     * Checks if User has an ID. If so, the user is valid and not an empty shell.
     * @return Always returns true or false
     */

    public String getFirstName() {
        return this.first_name;
    }

    public String getLastName() {
        return this.last_name;
    }

    public String getFullName() {
        return String.format("%s %s", this.first_name, this.last_name);
    }

    public String getImage() {
        return String.format("https://www.gravatar.com/avatar/%s", Hash.md5(this.email));
    }

    public String getEmail() {
        return this.email;
    }

    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }

    public void setLastName(String last_name) {
        this.last_name = last_name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUser(String first_name, String last_name, String email) {
        this.setFirstName(first_name);
        this.setLastName(last_name);
        this.setEmail(email);
    }

    public String getPasswordReset() {
        return this.password_reset;
    }
    private String hashPassword(String raw_password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(raw_password);
    }

    public void setPassword(String raw_password) {
        this.password = this.hashPassword(raw_password);
    }

    public boolean matchesPassword(String password) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.matches(password, this.password);
    }

    public void setPasswordReset(Date password_reset) {
        this.password_reset = password_reset.getOutput();
    }

    public void resetPasswordReset() {
        this.password_reset = null;
    }

    public void createToken() {
        this.setPasswordReset(new Date().addHours(6));
        Console.info(this.password_reset);
    }

    public String getToken() {
        return User.hash(String.format("%s-%s", this.email, this.password_reset));
    }

    public boolean matchesToken(String token) {
        return (Objects.equals(token, this.getToken()));
    }

    public boolean matchesEmailHash(String hashed_email) {
        return (Objects.equals(hashed_email, this.getHashedEmail()));
    }

    public String getHashedEmail() {
        return User.hash(this.email);
    }

    public void setActive() {
        this.active = true;
    }

    public boolean isActive() {
        return this.active;
    }

    public static String hash(String word) {
        return Hash.sha512(word);
    }

    static User clean() {
        return new User();
    }
}