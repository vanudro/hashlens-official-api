package nl.hashlens.api.models.users;

import nl.hashlens.api.asset.Console;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class Users {

    private static UserRepository users;

    @Autowired
    public Users(UserRepository users) {
        Console.info("[init user repository]");
        Users.users = users;
    }

    public static User getByID(String ID) {
        return getBy(users.findByID(ID));
    }

    public static User getByEmail(String email) {
        return getBy(users.findByEmail(email));
    }

    public static List<User> getAll() {
        return users.findAll();
    }

    public static User findUserByToken(String token) {
        return findUserBy("token", token);
    }

    public static User findUserByEmailHash(String hash) {
        return findUserBy("hash", hash);
    }

    public static User findUserBy(String find, String value) {
        User user = User.clean();
        for (User find_user : getAll()) {
            switch (find) {
                case "token":
                    if (find_user.matchesToken(value)) {
                        user = find_user;
                    }
                    break;
                case "hash":
                    if (find_user.matchesEmailHash(value)) {
                        user = find_user;
                    }
                    break;
            }
        }
        return user;
    }

    private static User getBy(List<User> haystack) {
        User user = User.clean();
        if (haystack.size() > 0) {
            for (User user_find : haystack) {
                user = user_find;
            }
        }
        return user;
    }

    public static Map<String, Object> getMap(User user) {
        Map<String, Object> map = new HashMap<>();
        map.put("ID", user.getID());
        map.put("first_name", user.getFirstName());
        map.put("last_name", user.getLastName());
        map.put("full_name", user.getFullName());
        map.put("email", user.getEmail());
        map.put("image", user.getImage());

        Map<String, Object> user_map = new HashMap<>();
        user_map.put("user", map);
        return user_map;
    }

    public static boolean save(User user) {
        if (user.isValid()) {
            users.save(user);
            return true;
        }
        return false;
    }

    public static boolean delete(User user, String password) {
        if (!user.matchesPassword(password)) {
            Console.info("Incorrect password, can't delete User...");
            return false;
        }
        try {
            users.delete(user);
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public static User create(String first_name, String last_name, String email, String password) {
        return new User(first_name, last_name, email, password);
    }

}
