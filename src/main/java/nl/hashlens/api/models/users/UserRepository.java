package nl.hashlens.api.models.users;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

interface UserRepository extends MongoRepository<User, String> {

    List<User> findByID(String ID);
    List<User> findByEmail(String email);

}
