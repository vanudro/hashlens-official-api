package nl.hashlens.api.models.channels;

import nl.hashlens.api.asset.Word;

import java.util.Objects;

class ChannelStatus {

    private Channel channel;

    public enum Status {OFFLINE, ERROR, WARNING, ACTIVE}

    private Status status;
    private String message;

    ChannelStatus(Channel channel) {
        this.status = this.getChannelStatus(channel, channel.getAccessToken());
    }

    public String getStatus() {
        return this.status.toString().toLowerCase();
    }
    public String getStatusName() {
        return Word.capitalize(this.getStatus());
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private Status getChannelStatus(Channel channel, String access_token) {
        if (!channel.isActive()) {
            this.setMessage("Channel has been turned offline");
            return Status.OFFLINE;
        }
        if (Objects.equals(access_token, "") || access_token == null) {
            this.setMessage("No access token found");
            return Status.ERROR;
        }
        this.setMessage("All is working fine!");
        return Status.ACTIVE;
    }
}
