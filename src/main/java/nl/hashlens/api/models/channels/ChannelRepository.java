package nl.hashlens.api.models.channels;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

interface ChannelRepository extends MongoRepository<Channel, String> {

    List<Channel> findAll();

    List<Channel> findAllByID(String id);

    List<Channel> findAllByType(Channel.Type type);

    List<Channel> findAllByActive(boolean active);

}
