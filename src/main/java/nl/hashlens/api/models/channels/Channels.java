package nl.hashlens.api.models.channels;

import nl.hashlens.api.asset.Console;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class Channels {

    private static ChannelRepository channels;

    @Autowired
    public Channels(ChannelRepository channels) {
        Console.info("[init channel repository]");
        Channels.channels = channels;
    }

    private static Channel get(List<Channel> haystack) {
        Channel channel = Channel.clean();
        if (haystack.size() > 0) {
            for (Channel channel_found : haystack) {
                channel = channel_found;
            }
        }
        getMap(channel);
        return channel;
    }

    public static List<Channel> getAll() {
        return channels.findAll();
    }

    public static Channel getByType(Channel.Type type) {
        return get(channels.findAllByType(type));
    }

    public static Channel getByID(String id) {
        return get(channels.findAllByID(id));
    }

    public static List<Channel> getAllByActive(boolean active) {
        return channels.findAllByActive(active);
    }

    public static Channel create(Channel.Type type, String app_id, String secret, boolean active) {
        return new Channel(type, app_id, secret, active);
    }

    public static Map<String, Object> getMap(Channel channel) {
        ChannelStatus channel_status = new ChannelStatus(channel);
        Map<String, Object> map = new HashMap<>();
        map.put("id", channel.getID());
        map.put("name", channel.getName());
        map.put("type", channel.getTypeString());
        map.put("active", channel.isActive());
        map.put("app_id", channel.getAppId());
        map.put("secret", channel.getSecret());
        map.put("token", channel.getAccessToken());

        Map<String, Object> status = new HashMap<>();
        status.put("name", channel_status.getStatusName());
        status.put("type", channel_status.getStatus());
        status.put("message", channel_status.getMessage());
        map.put("status", status);

        Map<String, Object> channel_map = new HashMap<>();
        channel_map.put("channel", map);
        return channel_map;
    }

    public static boolean save(Channel channel) {
        if (channel.isValid()) {
            channels.save(channel);
            return true;
        }
        return false;
    }

    public static boolean delete(Channel channel) {
        if (channel.isValid()) {
            channels.delete(channel);
            return true;
        }
        return false;
    }
}
