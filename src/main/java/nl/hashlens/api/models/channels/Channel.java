package nl.hashlens.api.models.channels;

import nl.hashlens.api.asset.Word;
import nl.hashlens.api.models.DataModel;

public class Channel extends DataModel {

    public enum Type {NONE, FACEBOOK, INSTAGRAM, LINKEDIN, TWITTER}

    private String app_id, secret, access_token;
    private Type type = Type.NONE;
    private boolean active = false;

    private Channel() {
    }

    Channel(Type type, String app_id, String secret, boolean active) {
        this.setNewID();
        this.type = type;
        this.set(app_id, secret, active);
    }

    public void set(String app_id, String secret, boolean active) {
        this.app_id = app_id;
        this.secret = secret;
        this.active = active;
    }

    public String getName() {
        return Word.capitalize(this.getTypeString());
    }

    public String getTypeString() {
        return this.type.toString().toLowerCase();
    }

    public String getAppId() {
        return this.app_id;
    }

    public String getSecret() {
        return this.secret;
    }

    public String getAccessToken() {
        return this.access_token;
    }

    public Channel.Type getType() {
        return this.type;
    }

    public void setAccessToken(String access_token) {
        this.access_token = access_token;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return this.active;
    }

    public static Channel clean() {
        return new Channel();
    }
}