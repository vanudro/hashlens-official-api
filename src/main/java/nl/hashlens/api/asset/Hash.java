package nl.hashlens.api.asset;

import org.springframework.security.crypto.codec.Hex;

import javax.validation.constraints.NotNull;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hash {

    private String hash = null;

    // TODO: Read up about Mockito
    private Hash(String type, @NotNull String value) {
        try {
            MessageDigest digest = MessageDigest.getInstance(type);
            if (digest != null) {
                byte[] raw_hash = digest.digest(value.getBytes(StandardCharsets.UTF_8));
                this.hash = new String(Hex.encode(raw_hash));
            }
        } catch (NullPointerException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private String getHash() {
        return this.hash;
    }

    private static String hash(String type, String value) {
        return new Hash(type, value).getHash();
    }

    public static String sha512(String value) {
        return Hash.hash("SHA-512", value);
    }

    public static String md5(String value) {
        return Hash.hash("MD5", value);
    }
}
