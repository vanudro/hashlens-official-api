package nl.hashlens.api.asset;

import nl.hashlens.api.components.Mail;

import java.util.HashMap;
import java.util.Map;

public class MailTemplates {

    // TODO: Create more mails
    public static boolean sendPasswordRecovery(String address, String name, String subject, String link) {
        Map<String, String> variables = new HashMap<>();
        variables.put("greeting", String.format("Hello %s", name));
        variables.put("name", name);
        variables.put("title", subject);
        variables.put("link", link);
        return Mail.send(address, name, subject, Mail.getTemplate("passwordRecover", variables), true);
    }

    public static boolean sendUserActivation(String address, String name, String subject, String link) {
        Map<String, String> variables = new HashMap<>();
        variables.put("greeting", String.format("Hello %s", name));
        variables.put("name", name);
        variables.put("title", subject);
        variables.put("link", link);
        return Mail.send(address, name, subject, Mail.getTemplate("accountActivation", variables), true);
    }
}
