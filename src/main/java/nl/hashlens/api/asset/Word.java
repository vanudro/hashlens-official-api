package nl.hashlens.api.asset;

public class Word {

    public static Object isNotNull(Object value, Object return_if_null) {
        return (value != null) ? value : return_if_null;
    }

    public static Object isNotNull(Object value) {
        return isNotNull(value, null);
    }

    public static boolean email(String email) {
        return email.matches("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w-]+\\.)+[\\w]+[\\w]$");
    }

    public static boolean isStrongPassword(String password) {
        return password.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$");
    }

    public static boolean hasNumbers(String value) {
        return value.matches("^(.*[\\d])$");
    }

    public static String capitalize(String string) {
        if (string == null || string.isEmpty()) {
            return string;
        }
        string = string.toLowerCase();
        return string.substring(0, 1).toUpperCase() + string.substring(1);
    }
}
