package nl.hashlens.api.asset;

public class Console {

    enum Type {ERROR, WARNING, SUCCESS, INFO}

    // Regular Colors
    private static final String BLACK = "\033[0;30m";   // BLACK
    private static final String RED = "\033[0;31m";     // RED
    private static final String GREEN = "\033[0;32m";   // GREEN
    private static final String YELLOW = "\033[0;33m";  // YELLOW
    private static final String BLUE = "\033[0;34m";    // BLUE
    private static final String PURPLE = "\033[0;35m";  // PURPLE
    private static final String CYAN = "\033[0;36m";    // CYAN
    private static final String WHITE = "\033[0m";   // WHITE

    // Bold
    private static final String BLACK_BOLD = "\033[1;30m";  // BLACK
    private static final String RED_BOLD = "\033[1;31m";    // RED
    private static final String GREEN_BOLD = "\033[1;32m";  // GREEN
    private static final String YELLOW_BOLD = "\033[1;33m"; // YELLOW
    private static final String BLUE_BOLD = "\033[1;34m";   // BLUE
    private static final String PURPLE_BOLD = "\033[1;35m"; // PURPLE
    private static final String CYAN_BOLD = "\033[1;36m";   // CYAN
    private static final String WHITE_BOLD = "\033[1;37m";  // WHITE

    private static void log(Type type, String message) {
        String color = getColor(type);
        System.out.println(String.format("%s %s HASHLENS %s%-10s%s %s", new Date("yyyy-mm-dd HH:mm:ss.SSS").getOutput(), PURPLE, color, type.toString(), WHITE, message));
    }

    private static String getColor(Type type) {
        switch (type) {
            case INFO:
                return BLUE;
            case ERROR:
                return RED;
            case SUCCESS:
                return GREEN;
            case WARNING:
                return YELLOW;
            default:
                return CYAN;
        }
    }

    public static void error(String message) {
        log(Type.ERROR, message);
    }

    public static void warning(String message) {
        log(Type.WARNING, message);
    }

    public static void success(String message) {
        log(Type.SUCCESS, message);
    }

    public static void info(String message) {
        log(Type.INFO, message);
    }

    public static void line() {
        System.out.println(String.format("%s%s  --------------------------------------------------------------------------", WHITE, new Date("yyyy-mm-dd HH:mm:ss.SSS").getOutput()));
    }

    public static void controllerPoint(String name) {
        System.out.println(String.format("%s %s HASHLENS %s%s", new Date("yyyy-mm-dd HH:mm:ss.SSS").getOutput(), PURPLE, CYAN_BOLD, name.toUpperCase()));
        line();
    }

    public static void print(String output) {
        System.out.println(String.format("%s  %s", new Date("yyyy-mm-dd HH:mm:ss.SSS").getOutput(), output));
    }
}