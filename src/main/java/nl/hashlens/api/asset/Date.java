package nl.hashlens.api.asset;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class Date {

    private static String FORMAT = "yyyy-MM-dd HH:mm:ss";

    private String format, output;

    public Date(String format, LocalDateTime date_time) {
        this.setFormat(format);
        this.setOutput(date_time);
    }

    public Date(String format) {
        this(format, LocalDateTime.now());
    }

    public Date() {
        this(FORMAT);
    }

    public Date(long stamp) {
        this(FORMAT, stamp);
    }

    public Date(String format, long stamp) {
        this(format, new Timestamp(stamp).toLocalDateTime());
    }

    public Date addHours(long add_hours) {
        this.setOutput(LocalDateTime.now().plusHours(add_hours));
        return this;
    }

    public String getOutput() {
        return this.output;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setOutput(String output) {
        this.output = output;
    }
    public void setOutput(LocalDateTime date_time) {
        this.setOutput(date_time.format(DateTimeFormatter.ofPattern(this.format)));
    }

    public boolean isAfter(String start, String is_after) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(FORMAT);
            java.util.Date d1 = sdf.parse(start);
            java.util.Date d2 = sdf.parse(is_after);

            return d1.after(d2);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }
}
