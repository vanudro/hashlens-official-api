package nl.hashlens.api.controller;

import nl.hashlens.api.asset.Console;
import nl.hashlens.api.asset.MailTemplates;
import nl.hashlens.api.asset.Word;
import nl.hashlens.api.models.users.User;
import nl.hashlens.api.models.users.Users;

import nl.hashlens.api.request.AccountLoginBody;
import nl.hashlens.api.request.AccountRecoverBody;
import nl.hashlens.api.request.AccountRegisterBody;
import nl.hashlens.api.request.UserPasswordBody;
import nl.hashlens.api.response.Response;
import org.springframework.web.bind.annotation.*;


@CrossOrigin
@RestController
@RequestMapping(value = "/account", produces = "application/json", consumes = "application/json")
public class AccountController {


    /**
     * @param body is an object with user data
     * @return Returns a JSON string with user data if correct
     */

    // TODO: Fix mapping in this controller
    @PostMapping("/register")
    public String createUser(@RequestBody AccountRegisterBody body) {
        Console.controllerPoint("Account register");
        Response response = new Response();

        // Get fields
        String first_name = body.getFirstName();
        String last_name = body.getLastName();
        String email = body.getEmail();
        String password = body.getPassword();
        String link = body.getLink();

        // Check fields
        if (first_name == null) {
            response.missingParameter("first_name");
        }
        if (last_name == null) {
            response.missingParameter("last_name");
        }
        if (email == null) {
            response.missingParameter("email");
        }
        if (password == null) {
            response.missingParameter("password");
        }
        if (link == null) {
            response.missingParameter("link");
        }
        // If missing fields return ERROR response
        if (response.hasErrors()) return response.get();

        if (email != null && !Word.email(email)) {
            response.notEmail("email");
        }
        if (password != null && !Word.isStrongPassword(password)) {
            response.addError("Password is not strong enough... Please make a new one (minimal 6 characters with 1 capital letter and 1 number)", "password");
        }
        // Check if not existing user
        if (Users.getByEmail(email).isValid()) {
            response.addError("We already have an account by that email... Please choose another address or go to \"Forgot password\"", "email");
        }
        if (response.hasErrors()) return response.get();

        // Save user
        User user = Users.create(first_name, last_name, email, password);

        // Send mail
        try {
            MailTemplates.sendUserActivation(email, first_name, "Account activation", link + "?token=" + user.getHashedEmail());
        } catch (Exception e) {
            e.printStackTrace();
            response.addError("Error while sending mail", "email");
        }
        if (response.hasErrors()) return response.get();

        if (!Users.save(user)) {
            response.addError("Something went wrong");
        }
        if (response.hasErrors()) return response.get();

        Console.success("User created");
        response.addInfo("You are almost done. Check your e-mail to activate your account", "email");
        response.addData(Users.getMap(user));
        return response.get();
    }

    @PostMapping("/login")
    public String loginAccount(@RequestBody AccountLoginBody body) {
        Console.controllerPoint("Account login");
        Response response = new Response();
        String email = body.getEmail();
        String password = body.getPassword();

        if (email == null) {
            response.missingParameter("email");
        }
        if (password == null) {
            response.missingParameter("password");
        }
        // If checks go wrong
        if (response.hasErrors()) return response.get();

        User user = Users.getByEmail(email);
        if (user == null || !user.matchesPassword(password)) {
            response.addError("Email is incorrect", "email");
            response.addError("Password is incorrect", "password");
        }
        // If checks go wrong
        if (response.hasErrors()) return response.get();

        if (user != null && !user.isActive()) {
            response.addWarning("Sorry, you still need to verify your email before you can login. Please check your mail.", "email");
        }
        // If checks go wrong
        if (response.hasErrors()) return response.get();

        response.addSuccess("Login successful!");
        response.addData(Users.getMap(user));
        return response.get();
    }

    @GetMapping("/validate/{hash}")
    public String validateAccount(@PathVariable String hash) {
        Console.controllerPoint("Account hash validate");
        Response response = new Response();
        if (hash == null) {
            response.addError("Missing hash... can't retrieve info...");
        }
        User user = Users.findUserByEmailHash(hash);
        if (user == null || !user.isValid()) {
            response.addError("Your account couldn't be found... Please register again.");
        }
        // If checks go wrong
        if (response.hasErrors()) return response.get();
        if (user.isActive()) {
            response.addInfo("Your account is already ACTIVE");
        } else {
            user.setActive();
            Users.save(user);
            response.addSuccess("Your account is now ACTIVE");
            response.addData(Users.getMap(user));
        }
        return response.get();
    }

    @PostMapping("/password/forgot")
    public String sendRecoveryMail(@RequestBody AccountRecoverBody body) {
        Console.controllerPoint("Account recovery mail");
        Response response = new Response();

        String email = body.getEmail();
        String link = body.getLink();
        if (email == null) {
            response.missingParameter("email");
        }
        if (link == null) {
            response.missingParameter("link");
        }
        if (response.hasErrors()) return response.get();

        User user = Users.getByEmail(email);
        if (user.isValid()) {
            user.createToken();
            Console.info(user.getPasswordReset());
            Console.info(user.getToken());

            try {
                MailTemplates.sendPasswordRecovery(email, user.getFirstName(), "Password recovery", link + "?token=" + user.getToken());
            } catch (Exception e) {
                e.printStackTrace();
                response.addError("Couldn't send mail", "email");
            }
            if (response.hasErrors()) return response.get();

            Users.save(user);
        }
        response.addSuccess("Email was send");
        return response.get();
    }

    // TODO: To new response
    @PutMapping("/password/new/{token}")
    public String updatePassword(@PathVariable String token, @RequestBody UserPasswordBody body) {
        Console.controllerPoint("Account new password");
        Response response = new Response();
        String password = body.getPassword();

        if (token == null) {
            response.missingParameter("token", true);
        }
        if (password == null) {
            response.missingParameter("password");
        }
        if (response.hasErrors()) return response.get();

        if (password != null && !Word.isStrongPassword(password)) {
            response.addError("Password isn't strong enough.", "password");
        }

        User user = Users.findUserByToken(token);
        if (!user.isValid()) {
            Console.error(String.format("Unknown token [%s]", token));
            response.addError("It seems like you used an invalid token... Please go back to the login page and request a new password.", "token");
        }
        if (response.hasErrors()) return response.get();

        if (user.matchesPassword(password)) {
            Console.error(String.format("Old password [%s]", token));
            response.addError("Sorry... this is your current password. Please choose a new one.", "password");
        }
        if (response.hasErrors()) return response.get();

        user.setPassword(password);
        user.resetPasswordReset();
        if (!Users.save(user)) {
            Console.error("Error while saving user");
            response.exceptionError();
        } else {
            response.addSuccess("Email was send");
        }
        return response.get();
    }
}

