package nl.hashlens.api.controller;

import nl.hashlens.api.response.Response;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/", produces = "application/json")
public class MainController {

    @GetMapping("/status")
    public String status() {
        Response response = new Response();
        response.setMessage("All is good!");
        return response.get();
    }
}
