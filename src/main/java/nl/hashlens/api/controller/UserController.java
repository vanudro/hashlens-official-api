package nl.hashlens.api.controller;


import nl.hashlens.api.asset.Console;
import nl.hashlens.api.asset.Word;
import nl.hashlens.api.models.users.User;
import nl.hashlens.api.models.users.Users;
import nl.hashlens.api.request.UserNamesBody;
import nl.hashlens.api.request.UserPasswordOldBody;
import nl.hashlens.api.request.UserPasswordBody;
import nl.hashlens.api.response.Response;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping(path = "/user", produces = "application/json", consumes = "application/json")
public class UserController {

    @PostMapping("/find/{id}")
    public String read(@PathVariable String id) {
        Console.controllerPoint("User read");
        Response response = new Response();
        if (id == null) {
            response.missingParameter("id", true);
        }
        User user = Users.getByID(id);
        if (!user.isValid()) {
            response.notFound("User");
        } else {
            Console.success("Loading user");
            response.addMessage(Response.MessageType.Success, "User found");
            response.addData(Users.getMap(user));
        }
        return response.get();
    }

    @PutMapping("/update/{id}")
    public String create(@PathVariable String id, @RequestBody UserNamesBody body) {
        Console.controllerPoint("User update");
        Response response = new Response();

        String first_name = body.getFirstName();
        String last_name = body.getLastName();
        String email = body.getEmail();

        // Getting body
        if (id == null) {
            response.missingParameter("id", true);
        }
        if (first_name == null) {
            response.missingParameter("first_name");
        }
        if (last_name == null) {
            response.missingParameter("last_name");
        }
        if (email == null) {
            response.missingParameter("email");
        }
        // Check if fields are not null
        if (response.hasErrors()) return response.get();


        // Check fields
        if (first_name != null && Word.hasNumbers(first_name)) {
            response.addMessage(Response.MessageType.Error, "A name can't have number in it", "first_name");
        }
        if (last_name != null && Word.hasNumbers(last_name)) {
            response.addMessage(Response.MessageType.Error, "A name can't have number in it", "last_name");
        }
        if (email != null && !Word.email(email)) {
            response.addMessage(Response.MessageType.Error, "Please enter a valid email address", "email");
        }
        User check_user = Users.getByEmail(email);
        if (check_user.isValid() && !check_user.matchesID(id)) {
            response.addMessage(Response.MessageType.Error, "That email is already in use.", "email");
        }
        // Check if fields are correct
        if (response.hasErrors()) return response.get();


        // Set new values on user
        User user = Users.getByID(id);
        user.setUser(first_name, last_name, email);
        if (!user.isValid()) {
            Console.error(String.format("Error while updating a user [User:%s]", id));
            response.addMessage(Response.MessageType.Error, "Something went wrong");
            return response.get();
        }

        // Save user
        if (!Users.save(user)) {
            Console.error(String.format("Tried updating a user [User:%s]", id));
            response.addMessage(Response.MessageType.Error, "Something went wrong");
            return response.get();
        }

        Console.success("Updating user");
        response.changesSaved();
        response.addData(Users.getMap(user));
        return response.get();
    }


    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable String id, @RequestBody UserPasswordBody body) {
        Console.controllerPoint("User delete");
        Response response = new Response();

        String password = body.getPassword();

        if (password == null) {
            response.missingParameter("password");
        }

        User user = Users.getByID(id);
        if (user.isValid() && !user.matchesPassword(password)) {
            response.addMessage(Response.MessageType.Error, "Password doesn't match current password", "password");
        }

        if (user.isValid() && !response.hasErrors()) {
            if (Users.delete(user, password)) {
                Console.info("Deleting user");
                response.addMessage(Response.MessageType.Success, "User was deleted");
                response.addData(Users.getMap(user));
            } else {
                Console.warning(String.format("Tried deleting a user [User:%s]", id));
                response.addMessage(Response.MessageType.Error, "Something went wrong");
            }
        } else {
            Console.warning(String.format("Error while deleting a user [User:%s]", id));
            response.addMessage(Response.MessageType.Error, "Something went wrong");
        }

        return response.get();
    }

    @PutMapping("/password/change/{id}")
    public String passwordChange(@PathVariable String id, @RequestBody UserPasswordOldBody body) {
        Console.info(String.format("Password update [User:%s]", id));
        Console.line();
        Response response = new Response();
        if (id == null) {
            response.missingParameter("id", true);
        }
        if (body.getOldPassword() == null) {
            response.missingParameter("old_password");
        }
        if (!Word.isStrongPassword(body.getPassword())) {
            response.addMessage(Response.MessageType.Error, "Your password is not strong enough", "password");
        }
        if (body.getPassword() == null) {
            response.missingParameter("password");
        }
        if (response.hasErrors()) return response.get();

        User user = Users.getByID(id);
        if (user.isValid() && !user.matchesPassword(body.getOldPassword())) {
            response.addMessage(Response.MessageType.Error, "Password doesn't match current password", "old_password");
        }
        if (!user.isValid()) {
            Console.warning(String.format("Error while updating a user's password [User:%s]", id));
            response.addMessage(Response.MessageType.Error, "Something went wrong");
        }
        if (response.hasErrors()) return response.get();

        user.setPassword(body.getPassword());
        if (Users.save(user)) {
            Console.success("Updating password");
            response.addMessage(Response.MessageType.Success, "User was updated");
            response.addData(Users.getMap(user));
        } else {
            Console.warning(String.format("Tried updating a user's password [User:%s]", id));
            response.addMessage(Response.MessageType.Error, "Something went wrong");
        }

        return response.get();
    }
}
