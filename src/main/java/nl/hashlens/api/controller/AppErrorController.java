package nl.hashlens.api.controller;

import nl.hashlens.api.response.Response;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class AppErrorController implements ErrorController {

    @ExceptionHandler
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public String mainOutput() {
        Response response = new Response();
        response.setMessage("404 NOT FOUND");
        response.addError("The current path is not recognised.");
        return response.get();
    }

    @RequestMapping("/error")
    @ResponseBody
    public String error(HttpServletRequest request) {
        Response response = new Response();
        response.unknownPost();
        return response.get();
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
