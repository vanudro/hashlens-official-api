package nl.hashlens.api.controller;

import nl.hashlens.api.asset.Console;
import nl.hashlens.api.asset.Word;
import nl.hashlens.api.components.Properties;
import nl.hashlens.api.models.channels.Channel;
import nl.hashlens.api.models.channels.Channels;
import nl.hashlens.api.request.ChannelAccessTokenBody;
import nl.hashlens.api.request.ChannelAuthorizeBody;
import nl.hashlens.api.request.ChannelBody;
import nl.hashlens.api.request.ChannelCreateBody;
import nl.hashlens.api.response.Response;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@CrossOrigin
@RestController
@RequestMapping(path = "/channel", produces = "application/json", consumes = "application/json")
public class ChannelController {

    @PostMapping("/create")
    public String create(@RequestBody ChannelCreateBody body) {
        Console.controllerPoint("Channel create");
        Response response = new Response();

        String type_value = body.getType();
        String app_id = body.getAppId();
        String secret = body.getSecret();
        boolean active = body.isActive();

        // Check given fields
        if (type_value == null) {
            response.missingParameter("type");
        }
        if (app_id == null) {
            response.missingParameter("app_id");
        }
        if (secret == null) {
            response.missingParameter("secret");
        }
        if (response.hasErrors()) return response.get();

        // Check given fields
        if (Objects.equals(type_value, "")) {
            response.emptyField("type");
        }
        if (Objects.equals(app_id, "")) {
            response.emptyField("app_id");
        }
        if (Objects.equals(secret, "")) {
            response.emptyField("secret");
        }

        if (response.hasErrors()) return response.get();

        Channel.Type type = Channel.Type.NONE;
        try {
            if (type_value != null) {
                type = Channel.Type.valueOf(type_value.toUpperCase());
            }
        } catch (Exception e) {
            response.addError("Unknown channel type", "type");
        }

        if (response.hasErrors()) return response.get();

        Channel find_channel = Channels.getByType(type);
        if (find_channel.isValid()) {
            response.addError("Channel already exists.", "type");
        }

        if (response.hasErrors()) return response.get();

        // Create channel
        Channel channel = Channels.create(type, app_id, secret, active);
        if (!Channels.save(channel)) {
            response.addError("Something went wrong...");
        } else {
            response.addSuccess("Channel created");
        }

        return response.get();
    }

    @PostMapping("/find/id/{id}")
    public String findById(@PathVariable String id) {
        Console.controllerPoint("Channel read");
        Response response = new Response();

        if (id == null) {
            response.missingParameter("id", true);
        }

        Channel channel = Channels.getByID(id);
        return this.getChannel(channel, response);
    }

    @PostMapping("/find/name/{name}")
    public String findByName(@PathVariable String name) {
        Console.controllerPoint("Channel read name");
        Response response = new Response();

        if (name == null) {
            response.missingParameter("id", true);
        }

        Channel.Type type = Channel.Type.NONE;
        try {
            type = Channel.Type.valueOf(name != null ? name.toUpperCase() : null);
        } catch (Exception e) {
            e.printStackTrace();
            response.addError("Channel type doesn't exist", "type");
        }

        Channel channel = Channels.getByType(type);
        return this.getChannel(channel, response);
    }

    public String getChannel(Channel channel, Response response) {
        if (!channel.isValid()) {
            response.addError("Channel doesn't exist...", "type");
        }
        if (response.hasErrors()) return response.get();

        response.addSuccess("Channel found");
        response.addData(Channels.getMap(channel));

        return response.get();

    }

    @PostMapping("/get/list")
    public String getAll() {
        Console.controllerPoint("Channel list");
        Response response = new Response();
        for (Channel channel : Channels.getAll()) {
            response.addData(Channels.getMap(channel));
        }
        return response.get();
    }

    @GetMapping("/get/types")
    public String types() {
        Console.controllerPoint("Channel get types");
        Response response = new Response();

        List<String> list = new ArrayList<>();
        for (Channel.Type type : Channel.Type.values()) {
            if (type != Channel.Type.NONE) {
                list.add(Word.capitalize(type.name()));
            }
        }
        response.addData(list);

        return response.get();
    }

    @PutMapping("/save/{name}")
    public String save(@PathVariable String name, @RequestBody ChannelBody body) {
        Console.controllerPoint("Channel save");
        Response response = new Response();

        String app_id = body.getAppId();
        String secret = body.getSecret();
        boolean active = body.isActive();

        // Check given fields
        if (app_id == null) {
            response.missingParameter("app_id");
        }
        if (secret == null) {
            response.missingParameter("secret");
        }
        if (response.hasErrors()) return response.get();

        // Check given fields
        if (Objects.equals(app_id, "")) {
            response.emptyField("app_id");
        }
        if (Objects.equals(secret, "")) {
            response.emptyField("secret");
        }

        if (response.hasErrors()) return response.get();

        Channel.Type type = Channel.Type.NONE;
        try {
            type = Channel.Type.valueOf(name.toUpperCase());
        } catch (Exception e) {
            e.printStackTrace();
            response.addError(String.format("Unknown channel type '%s'", type.toString().toLowerCase()), "type");
        }

        if (response.hasErrors()) return response.get();

        Channel channel = Channels.getByType(type);
        if (response.hasErrors()) return response.get();

        // Create channel
        channel.set(app_id, secret, active);
        if (!Channels.save(channel)) {
            response.addError("Something went wrong...");
        } else {
            response.addSuccess("Channel saved!");
        }
        return response.get();
    }

    @PostMapping("/toggle/{id}")
    public String toggle(@PathVariable String id) {
        Console.controllerPoint("Channel toggle");
        Response response = new Response();
        if (id == null) {
            response.missingParameter("id", true);
        }
        if (response.hasErrors()) return response.get();
        Channel channel = Channels.getByID(id);
        if (!channel.isValid()) {
            response.addError("Channel not found", "id");
        }
        if (response.hasErrors()) return response.get();

        channel.setActive(!channel.isActive());
        if (!Channels.save(channel)) {
            response.addError("Something went wrong");
        } else {
            response.addSuccess(String.format("Toggled channel to %s", channel.isActive()));
            response.addData(Channels.getMap(channel));
        }
        return response.get();
    }

    @PostMapping("/get/auth_url/{id}")
    public String authorizeURL(@PathVariable String id, @RequestBody ChannelAuthorizeBody body) {
        Console.controllerPoint("Channel authorize URL");
        Response response = new Response();
        String link = body.getLink();
        if (id == null) {
            response.missingParameter("name", true);
        }
        if (link == null) {
            response.missingParameter("link");
        }
        if (response.hasErrors()) return response.get();
        Channel channel = Channels.getByID(id);
        if (!channel.isValid()) {
            response.addError("Channel not found", "name");
        }
        if (response.hasErrors()) return response.get();
        String url = "";

        try {
            String property = null;
            switch (channel.getType()) {
                case FACEBOOK:
                    property = Properties.FACEBOOK_AUTH_URL;
//                    url = String.format(Properties.get(Properties.FACEBOOK_AUTH_URL), channel.getAppId(), link);
                    break;
                case INSTAGRAM:
                    property = Properties.INSTAGRAM_AUTH_URL;
//                    url = String.format(Properties.get(Properties.INSTAGRAM_AUTH_URL), channel.getAppId(), link);
                    break;
                case LINKEDIN:
                    property = Properties.LINKEDIN_AUTH_URL;
//                    url = String.format(Properties.get(Properties.INSTAGRAM_AUTH_URL), channel.getAppId(), link);
                    break;
            }
            if (property != null) {
                url = String.format(Properties.get(property), channel.getAppId(), link);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Console.print(e.getMessage());
            response.addError("Error while parsing URL");
        }
        if (response.hasErrors()) return response.get();

        if (Objects.equals(url, "")) {
            response.addError(String.format("%s doesn't have a URL format yet...", channel.getName()));
        }
        if (response.hasErrors()) return response.get();

        response.addSuccess("Fetched authorize URL");
        response.addData(url);
        Console.print(url);
        return response.get();
    }

    @PutMapping("/put/token/{id}")
    public String token(@PathVariable String id, @RequestBody ChannelAccessTokenBody body) {
        Response response = new Response();
        String token = body.getToken();
        if (id == null) {
            response.missingParameter("id", true);
        }
        if (token == null) {
            response.missingParameter("token");
        }
        if (response.hasErrors()) return response.get();

        Channel channel = Channels.getByID(id);
        Console.print(id);
        if (!channel.isValid()) {
            response.addError("Channel doesn't exist.");
        }
        if (response.hasErrors()) return response.get();
        channel.setAccessToken(token);

        if (!Channels.save(channel)) {
            response.addError("Something went wrong");
        } else {
            response.addSuccess("Access token saved!");
            response.addData(Channels.getMap(channel));
        }
        return response.get();
    }

    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable String id) {
        Response response = new Response();
        if (id == null) {
            response.missingParameter("id", true);
        }
        if (response.hasErrors()) return response.get();
        Console.print(id);
        Channel channel = Channels.getByID(id);
        if (!channel.isValid()) {
            response.addError("Channel not found", "id");
        }
        if (response.hasErrors()) return response.get();

        if (!Channels.delete(channel)) {
            response.addError("Something went wrong");
        } else {
            response.addInfo("Channel deleted...");
            response.addData(Channels.getMap(channel));
        }
        return response.get();
    }

}
