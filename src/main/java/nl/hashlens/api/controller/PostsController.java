package nl.hashlens.api.controller;

import nl.hashlens.api.asset.Console;
import nl.hashlens.api.models.channels.Channel;
import nl.hashlens.api.models.channels.Channels;
import nl.hashlens.api.models.posts.Post;
import nl.hashlens.api.models.posts.Posts;
import nl.hashlens.api.response.Response;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@CrossOrigin
@RestController
@RequestMapping(path = "/posts", produces = "application/json")
public class PostsController {

    private Map<String, Post> posts = null;

    private void initPosts() {
        if (this.posts == null) {
            this.posts = new HashMap<>();
        }
    }

    @GetMapping("/all")
    public String all() {
        Console.controllerPoint("Posts all");
        Response response = new Response();

        return response.get();
    }

    @GetMapping("/from/{type}")
    public String from(@PathVariable String type) {
        Console.controllerPoint("Posts from type");
        Response response = new Response();

        if (type == null) {
            response.missingParameter("type", true);
        }
        if (response.hasErrors()) return response.get();
        Channel.Type channel_type = Channel.Type.NONE;
        try {
            channel_type = type != null ? Channel.Type.valueOf(type.toUpperCase()) : Channel.Type.NONE;
        } catch (Exception e) {
            response.addError("Wrong channel type found.");
        }
        if (response.hasErrors()) return response.get();

        Channel channel = Channels.getByType(channel_type);
        if (!channel.isValid()) {
            response.addError("Channel found.");
        }
        if (response.hasErrors()) return response.get();

        return this.getPostsFromChannel(channel);
    }

    private String getPostsFromChannel(Channel channel) {
        Map<String, Post> posts = Posts.findByChannel(channel);
        Map<String, Object> posts_raw = new HashMap<>();

        for (Map.Entry<String, Post> entry : posts.entrySet()) {
            posts_raw.put(entry.getKey(), entry.getValue().getMap());
        }
        return this.returnPosts(posts_raw);
    }

    private String returnPosts(Map<String, Object> posts) {
        Response response = new Response();

        Map<String, Object> map = new TreeMap<>(posts);
        int count = 0;
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            Console.print(String.valueOf(count));
            response.addData(entry.getKey(), entry.getValue());
        }

        return response.get();
    }

}
