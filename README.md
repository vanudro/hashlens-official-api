# Hashlens API

The Hashlens API is a part of the Hashlens system which does al the heavy work. This is basically the heart of everything and without this you cant get any data.

## Requirements
To install the Hashlens API you'll need:
- [Java 8](https://www.java.com/nl/download/) installed
- MongoDB installed. info at [MongoDB Docs](https://docs.mongodb.com/)
- a mail server. You can also use GMAIL for this. [Example](https://www.siteground.com/kb/google_free_smtp_server/)
- and a JAR file which you can compile from this GIT.

## Installation
Execute the JAR file with the following arguments

```bash
java -jar hashlens-official-<version>.jar --dbhost=<host> --dbport=<port> --dbauth=<user_table> --dbuser=<user> --dbpass=<password> --dbname=<db_name> --mailhost=<mail_host> --mailuser=<mail_user> --mailpass=<mail_password> --mailport=<mail_port>
```
The arguments to start the JAR file are all needed. Without these the startup of Hashlens will result in an Exception error.

###DB config
These arguments contain the parameters for the MongoDB login.<br />
```
 --dbhost=localhost
 --dbport=27017
 --dbauth=admin
 --dbuser=root 
 --dbpass=ExtremelyDifficultPassword123#@!
 --dbname=hashlens-official
 ```

 ### Mail config
 and these parameters are for the build-in mail service. (For example gmail)
 ```
 --mailhost=smtp.gmail.com
 --mailuser=example@gmail.com
 --mailpass=AgainExtremelyDifficultPassword123#@!
 --mailport=465
 ```
 
## Usage
 When the installation is done you can start using the REST API by making calls to specific endpoints. These points are documented bij Swagger UI.
 If the app is running you can locate the documentation at: <br />
 ``` http://www.example.com:<port>/swagger-ui.html```
 

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate and try not to break to much like I did.

## Disclosure
This code was developed as an intern assignment at the start of September 2019 until the end of January 2020.

## Contact
For more info, send me a message at [robin.vanuden@ida-mediafoundry.nl](robin.vanuden@ida-mediafoundry.nl)
 
 
